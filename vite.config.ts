import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

export default defineConfig({
  plugins: [react()],
  base: '/phaser-truth-or-dare/',
  server: {
    port: 8088,
  }
})
