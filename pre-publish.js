// GENERATE NEW PACKAGE.JSON FILE FOR LIBRARY PURPOSE
const fs = require('fs');
const _ = require('lodash');
let currentVersion = {}
try {
    currentVersion = require('./current-version.json');
    console.log(currentVersion);    
} catch (error) {
    console.log('No current version file');
}
const LIBRARY_DATA = {
    "files": [
        "*"
      ],
      "types": "./src/index.d.ts",
      "main": "./truth-or-dare-lib.umd.js",
      "module": "./truth-or-dare-lib.es.js",
      "exports": {
        ".": {
          "import": "./truth-or-dare-lib.es.js",
          "require": "./truth-or-dare-lib.umd.js"
        }
      },
}
const currentPackageJson = require('./package.json');

let newPackageJson = _.cloneDeep(currentPackageJson);

newPackageJson.version = currentVersion.version

newPackageJson = {
    ...newPackageJson,
    ...LIBRARY_DATA
}

const REACT_DEP_KEY = "react";
const REACT_DOM_DEP_KEY = "react-dom"
const TYPE_REACT_DEV_DEP_KEY = "@types/react";
const TYPE_REACT_DOM_DEV_DEP_KEY = "@types/react-dom"

const { 
    [REACT_DEP_KEY]: reactDepValue, 
    [REACT_DOM_DEP_KEY]: reactDomDepValue, 
    ...leftDep
} = currentPackageJson.dependencies;
newPackageJson.dependencies = _.cloneDeep(leftDep);

const { 
    [TYPE_REACT_DEV_DEP_KEY]: reactDevDepValue, 
    [TYPE_REACT_DOM_DEV_DEP_KEY]: reactDomDevDepValue,
    ...leftDevDep
} = currentPackageJson.devDependencies;

newPackageJson.peerDependencies = {
    [REACT_DEP_KEY]: reactDepValue, 
    [REACT_DOM_DEP_KEY]: reactDomDepValue, 
    [TYPE_REACT_DEV_DEP_KEY]: reactDevDepValue, 
    [TYPE_REACT_DOM_DEV_DEP_KEY]: reactDomDevDepValue,
}

delete newPackageJson.scripts;
delete newPackageJson.devDependencies;

fs.writeFile('package.lib.json', JSON.stringify(newPackageJson, null, 2), function(err) {
    if(err) {
        return console.log(err);
    }
    console.log("The file (package.lib.json) was created successfully!");
}); 