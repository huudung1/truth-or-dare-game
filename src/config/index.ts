export const CONFIG = {
    API_DOMAIN_URL: 'http://localhost:3000',
    SOCKET_DOMAIN_URL: 'ws://localhost:3000',
}

export const COMMAND_TYPE = {
    READY_TO_START_GAME: 'todgame/vote',
    SELECT_TRUTH_OR_DARE: 'todgame/truth_or_dare',
    ACCEPT_USER_ANSWER: 'todgame/vote',
}

export const TRUTH_OR_DARE = {
    TRUTH: 1,
    DARE: 2,
}

export const TRUTH_OR_DARE_OPTION = {
    GET_FROM_BANK: 1,
    GET_FROM_OTHER_USERS: 0,
}

export const COMMAND_VOTE = {
    ACCEPT: 1,
    DECLINE: 0,
}

export const SOCKET_EVENT_NAME = {
    CONNECT: 'connect',
    RANDOM_MEMBER: 'todgame/random_member',
    USER_SELECTED_TRUTH_OR_DARE: 'todgame/truth_or_dare',
    USER_VOTE_ACCEPT: 'todgame/vote',
}