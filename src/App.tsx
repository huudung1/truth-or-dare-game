import { getQueryStringParams } from "./helpers";
import TruthOrDare from "./TruthOrDare";

const App = (): JSX.Element => {
    const params = getQueryStringParams();
    const authUserId = String(params.authUserId);
    const token = String(params.token);

    return (
        <>
            <TruthOrDare
                token={token}
                roomId="3fbfda07-cacd-4d7f-8a71-dd8687ff0561"
                authUserId={authUserId}
                workspaceId="36c5d2b1-0235-487b-bed9-7c2a29469165"
            />
        </>
    )
}

export default App;