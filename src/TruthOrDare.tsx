import React from 'react';
import Service from './services';
import Socket from './services/socket';

// styles
import styles from './app.module.scss'

// layouts
import Main from './layouts/Main'
import RoomInfo from './layouts/RoomInfo'
import VoteToStart from './layouts/VoteToStart';
import { SOCKET_EVENT_NAME } from './config';
import { parseMemberFromResponse } from './helpers';

interface IAppProps {
  token: string,
  roomId: string,
  authUserId: string,
  workspaceId: string,
}

const TruthOrDare = ({ token, roomId, authUserId, workspaceId }: IAppProps) => {
  const [isReady, setIsReady] = React.useState<boolean>(false);
  const [roomInfo, setRoomInfo] = React.useState<RoomNS.Info | undefined>();
  const [playingMember, setPlayingMember] = React.useState<RoomNS.Member | undefined>();

  const setupToken = async (): Promise<void> => {
    await Service.getInstance().setToken(token);
  }

  const getRoomInfo = async (): Promise<void> => {
    const res = await Service.getInstance().getRoomInfo(roomId);
    setRoomInfo(res);
  }

  const connectSocket = (): void => {
    Socket.getInstance().connect(authUserId, token, workspaceId);
  }

  const registerSocketEvents = (): void => {
    const listener = Socket.getInstance().listener;

    listener.on(SOCKET_EVENT_NAME.CONNECT, () => {
      listener.on(SOCKET_EVENT_NAME.RANDOM_MEMBER, (data: Record<string, unknown>) => {
        const { user } = data;
        if (user) setPlayingMember(parseMemberFromResponse(Object(user)));
        setTimeout(() => {
          setIsReady(true);
        }, 1000);
      })
    });
  }

  React.useEffect(() => {
    setupToken().then(() => {
      connectSocket();
      registerSocketEvents();
      getRoomInfo();
    });
  }, []);

  if (!roomInfo) return null;

  return (
    <div className={styles.appContainer}>
      <RoomInfo data={roomInfo} />
      {!isReady
        ?
        <VoteToStart roomId={roomId} />
        :
        <Main
          roomId={roomId}
          authUserId={authUserId}
          playingMember={playingMember}
        />}
    </div>
  )
}

export default TruthOrDare
