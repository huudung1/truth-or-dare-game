import React from 'react'
import Avatar from '../../components/Avatar';
import Tooltip from '../../components/Tooltip';

// resources
import InviteIcon from '../../assets/images/invite.svg';

// styles
import styles from './styles.module.scss'

interface IRoomInfoProps {
    data: RoomNS.Info,
}

// main
const RoomInfo = ({ data }: IRoomInfoProps): JSX.Element => {
    return (
        <div className={styles.container}>
            <div className={styles.mainContent}>
                <div className={styles.name}>{data.name}</div>
                <div className={styles.members}>
                    {
                        data.members.map(memb =>
                            <div className={styles.member} key={memb.id}>
                                <Avatar
                                    username={memb.username}
                                    url={memb.avatar}
                                />
                            </div>
                        )
                    }
                </div>
            </div>

            <div className={styles.actions}>
                <button className={styles.inviteButton}>
                    <img src={InviteIcon} alt="invite-icon" />
                    <Tooltip
                        className={styles.tooltip}
                        text="Invite friends"
                    />
                </button>
            </div>
        </div>
    )
}

export default RoomInfo;