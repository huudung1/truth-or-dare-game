import React from 'react'
import Avatar from '../../components/Avatar';
import Tooltip from '../../components/Tooltip';
import Service from '../../services';
import { COMMAND_VOTE, SOCKET_EVENT_NAME, TRUTH_OR_DARE } from '../../config';

// resources
import QuestionIcon from '../../assets/images/question-ic.png';

// styles
import styles from './styles.module.scss'
import Socket from '../../services/socket';
import { parseMemberFromResponse } from '../../helpers';

interface IMainProps {
    roomId: string,
    authUserId: string,
    playingMember?: RoomNS.Member,
}

// main
const Main = ({ roomId, authUserId, playingMember }: IMainProps): JSX.Element => {
    const [selectedOption, setSelectedOption] = React.useState<'truth' | 'dare' | ''>('');
    const [content, setContent] = React.useState<string>('');
    const [acceptedMembers, setAcceptedMembers] = React.useState<RoomNS.Member[]>([]);
    const [isShownTurnNotification, setIsShownTurnNotification] = React.useState<boolean>(false);
    const [isAcceptedPlayingMemberAnswer, setIsAcceptedPlayingMemberAnswer] = React.useState<boolean>(false);

    const timeoutHandlerRef = React.useRef<ReturnType<typeof setTimeout>>();

    const handleSelectOption = (type: number): void => {
        if (playingMember?.id !== authUserId) return;

        if (timeoutHandlerRef.current) clearTimeout(timeoutHandlerRef.current);

        Service.getInstance().chooseTruthOrDare(roomId, type);
        setTimeout(() => {
            Service.getInstance().voteAccept(roomId, COMMAND_VOTE.ACCEPT);
        }, 3000);
    }

    const registerSocketEvents = (): void => {
        const listener = Socket.getInstance().listener;

        listener.on(SOCKET_EVENT_NAME.USER_SELECTED_TRUTH_OR_DARE, (data: Record<string, unknown>) => {
            const { type, question } = data;
            setContent(String(question));
            setSelectedOption(type === TRUTH_OR_DARE.TRUTH ? 'truth' : 'dare');
        })

        listener.on(SOCKET_EVENT_NAME.USER_VOTE_ACCEPT, (data: Record<string, unknown>) => {
            const { user } = data;
            if (Object(user).id !== authUserId) {
                setAcceptedMembers(current => [
                    ...current,
                    parseMemberFromResponse(Object(user))
                ])
            }
        })

        listener.on(SOCKET_EVENT_NAME.RANDOM_MEMBER, () => {
            setSelectedOption('');
            setContent('');
            setAcceptedMembers([]);
            setIsAcceptedPlayingMemberAnswer(false);
            setIsShownTurnNotification(false);
        })
    }

    React.useEffect(() => {
        if (timeoutHandlerRef.current) clearTimeout(timeoutHandlerRef.current);

        if (playingMember?.id === authUserId) {
            timeoutHandlerRef.current = setTimeout(() => {
                setIsShownTurnNotification(true);

                setTimeout(() => {
                    setIsShownTurnNotification(false);
                }, 2000);
            }, 800);
        }
    }, [playingMember])

    React.useEffect(() => {
        registerSocketEvents();
    }, []);

    return (
        <div className={styles.container}>
            {
                !selectedOption
                    ?
                    <div className={styles.options}>
                        <button onClick={() => {
                            handleSelectOption(TRUTH_OR_DARE.TRUTH);
                        }}>Truth</button>
                        <button onClick={() => {
                            handleSelectOption(TRUTH_OR_DARE.DARE);
                        }}>Dare</button>
                    </div>
                    :
                    <div className={styles.quest}>
                        <div className={styles.title}>
                            {selectedOption === 'truth' ? 'Truth' : 'Dare'}
                        </div>

                        {content}
                    </div>
            }

            <div className={styles.footer}>
                <div className={styles.activeMember}>
                    <Avatar url={playingMember?.avatar} username={playingMember?.username || '--'} className={styles.avatar} />
                    <div className={styles.username}>{playingMember?.username || '--'}</div>
                    <div>{' \'s turn'}</div>
                </div>

                {
                    !!selectedOption
                        ?
                        <>
                            {
                                playingMember?.id !== authUserId
                                    ?
                                    <button
                                        className={isAcceptedPlayingMemberAnswer ? styles.noBorder : ''}
                                        onClick={() => {
                                            if (isAcceptedPlayingMemberAnswer) return;
                                            Service.getInstance().voteAccept(roomId, COMMAND_VOTE.ACCEPT);
                                            setIsAcceptedPlayingMemberAnswer(true);
                                        }}
                                    >{isAcceptedPlayingMemberAnswer ? 'Accepted' : `Accept the answer`}</button>
                                    :
                                    <div className={styles.acceptMembers}>
                                        {
                                            acceptedMembers.length === 0
                                                ?
                                                <span>Waiting for accepts...</span>
                                                :
                                                <>
                                                    <span>Accepted: </span>
                                                    <div className={styles.members}>
                                                        {
                                                            acceptedMembers.map(memb =>
                                                                <Avatar key={memb.id} url={memb.avatar} username={memb.username} className={styles.avatar} />
                                                            )
                                                        }
                                                    </div>
                                                </>
                                        }
                                    </div>
                            }
                        </>
                        :
                        <div className={styles.note}>
                            <img src={QuestionIcon} alt="question-icon" />

                            <Tooltip
                                className={styles.tooltip}
                                text="Please select Truth or Dare"
                            />
                        </div>
                }
            </div>

            {
                isShownTurnNotification
                &&
                <div className={styles.myTurn}>
                    <span>It's your turn</span>
                </div>
            }
        </div>
    )
}

export default Main;