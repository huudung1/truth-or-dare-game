import React from 'react'
import Service from '../../services';

// styles
import styles from './styles.module.scss'

// interface
interface IVoteToStartProps {
    roomId: string,
}

// main
const VoteToStart = ({roomId}: IVoteToStartProps): JSX.Element => {
    const [isReady, setIsReady] = React.useState<boolean>(false);

    return (
        <div className={styles.container}>
            <h3> Truth or Dare </h3>

            {
                !isReady
                    ?
                    <>
                        <div>Click ready to start playing</div>
                        <button onClick={() => {
                            Service.getInstance().voteReadyToPlay(roomId);
                            setIsReady(true);
                        }}>
                            Ready
                        </button>
                    </>
                    :
                    <div>Waiting for other players ...</div>
            }
        </div>
    )
}

export default VoteToStart;