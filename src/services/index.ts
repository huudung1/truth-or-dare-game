import { COMMAND_TYPE, COMMAND_VOTE, CONFIG, TRUTH_OR_DARE_OPTION } from "../config";
import { parseMemberFromResponse } from "../helpers";
import HTTP from "./http";

class Service {
    private static instance?: Service;
    private http: HTTP;

    constructor() {
        this.http = new HTTP;
    }

    public static getInstance(): Service {
        if (!Service.instance) Service.instance = new Service();

        return Service.instance;
    }

    public async setToken(tkn: string): Promise<void> {
        this.http.setToken(tkn);
    }

    public async getRoomInfo(roomId: string): Promise<RoomNS.Info> {
        const res = await this.http.get(`${CONFIG.API_DOMAIN_URL}/api/games/${roomId}`);

        const data = Object(res.data);

        return {
            id: roomId,
            name: data.name ? String(data.name) : 'Join with us',
            members: data.gameInfo && data.gameInfo.members ? data.gameInfo.members.map((item: Record<string, unknown>) => parseMemberFromResponse(item)) : data.hosts.map((item: Record<string, unknown>) => parseMemberFromResponse(item)),
            playingMember: data.gameInfo && data.gameInfo.user ? parseMemberFromResponse(data.gameInfo.user) : undefined
        }
    }

    public async voteReadyToPlay(roomId: string): Promise<void> {
        this.http.post(`${CONFIG.API_DOMAIN_URL}/api/games/${roomId}/command`, {
            command: {
                name: COMMAND_TYPE.READY_TO_START_GAME,
                data: {
                    vote: COMMAND_VOTE.ACCEPT,
                }
            }
        });
    }

    public async chooseTruthOrDare(roomId: string, type: number): Promise<void> {
        this.http.post(`${CONFIG.API_DOMAIN_URL}/api/games/${roomId}/command`, {
            command: {
                name: COMMAND_TYPE.SELECT_TRUTH_OR_DARE,
                data: {
                    type,
                    option: TRUTH_OR_DARE_OPTION.GET_FROM_BANK,
                }
            }
        })
    }

    public async voteAccept(roomId: string, vote: number): Promise<void> {
        this.http.post(`${CONFIG.API_DOMAIN_URL}/api/games/${roomId}/command`, {
            command: {
                name: COMMAND_TYPE.ACCEPT_USER_ANSWER,
                data: {
                    vote,
                }
            }
        })
    }
}

export default Service;