import axios from 'axios';

class HTTP {
    private token?: string;

    public setToken(tkn: string) {
        this.token = tkn;
    }


    public async get(url: string): Promise<Record<string, unknown>> {
        const res = await axios.get(url, {
            headers: {
                'Authorization': `Bearer ${this.token}`
            }
        });
        return res.data;
    }

    public async post(url: string, body: Record<string, unknown>): Promise<Record<string, unknown>> {
        const res = await axios.post(url, body, {
            headers: {
                'Authorization': `Bearer ${this.token}`
            }
        });

        return res.data;
    }

    public async put(url: string, body: Record<string, unknown>): Promise<Record<string, unknown>> {
        const res = await axios.put(url, body, {
            headers: {
                'Authorization': `Bearer ${this.token}`
            }
        });

        return res.data;
    }

    public async delete(url: string): Promise<Record<string, unknown>> {
        const res = await axios.delete(url, {
            headers: {
                'Authorization': `Bearer ${this.token}`
            }
        });

        return res.data;
    }

}

export default HTTP;