import io from 'socket.io-client';
import { CONFIG } from '../config';

class Socket {
    private static instance?: Socket;
    private socket?: any;

    public static getInstance() {
        if (!Socket.instance) Socket.instance = new Socket();

        return Socket.instance;
    }

    public get listener(): any {
        return this.socket;
    }

    public connect(userId: string, token: string, workspaceId: string): void {
        this.socket = io(`${CONFIG.SOCKET_DOMAIN_URL}/workspace-${workspaceId}`, {
            query: { userId, },
            auth: { token },
        });

        this.socket.onAny((eventName: string, data: any) => {
            console.log('socket', eventName, data);
        });
    }
}

export default Socket;