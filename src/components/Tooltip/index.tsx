import React from 'react'

// styles
import styles from './styles.module.scss'

// interfaces
interface ITooltipProps {
    className?: string,
    text: string,
}

// main
const Tooltip = ({ className, text }: ITooltipProps): JSX.Element => {
    return (
        <div className={`${styles.container} ${className}`}>
            {text}
        </div>
    )
}

export default Tooltip;