import React from 'react'
import Tooltip from '../Tooltip';

// styles
import styles from './styles.module.scss'

// interfaces
interface IAvatar {
    className?: string,
    username: string,
    url?: string,
}

// main
const Avatar = ({ className, username, url }: IAvatar): JSX.Element => {
    return (
        <div className={`${styles.container} ${className}`} style={{
            backgroundImage: `url(${url})`
        }}>
            {!url && username && username.substr(0, 1)}
            <Tooltip
                className={styles.tooltip}
                text={username}
            />
        </div>
    )
}

export default Avatar;