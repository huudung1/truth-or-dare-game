export const parseMemberFromResponse = (data: Record<string, unknown>): RoomNS.Member => {
    return {
        id: String(data.id),
        username: String(data.username),
        email: String(data.email),
        avatar: String(data.avatar),
    }
}

export const getQueryStringParams = (): Record<string, unknown> => {
    const query = window.location.search;

    return query
        ? (/^[?#]/.test(query) ? query.slice(1) : query)
            .split('&')
            .reduce((params, param) => {
                let [key, value] = param.split('=');
                (params as Record<string, unknown>)[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
                return params;
            }, {}
            )
        : {}
};