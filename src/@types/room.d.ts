declare namespace RoomNS {
    interface Member {
        id: string,
        username: string,
        email: string,
        avatar?: string,
    }
    
    interface Info {
        id: string,
        name: string,
        members: Member[],
        playingMember?: Member,
    }
}